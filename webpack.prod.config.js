// noinspection DuplicatedCode

const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const {resolve} = require('path')

const postcssLoader = {
  loader: 'postcss-loader',
  options: {
    postcssOptions: {
      plugins: [require('autoprefixer')]
    }
  }
}

const cssLoader = [
  // 'style-loader',
  {
    loader: MiniCssExtractPlugin.loader,
    options: {
      // publicPath: ''
    }
  },
  'css-loader',
  postcssLoader
]

module.exports = {
  mode: 'production',
  entry: './src/js/index',
  output: {
    path: resolve(__dirname, 'dest'),
    filename: 'index.js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?/i,
        exclude: /node_modules/,
        enforce: 'pre',
        use: 'eslint-loader'
      },
      {
        oneOf: [
          {
            test: /\.css$/,
            use: cssLoader
          },
          {
            test: /\.less$/i,
            use: [
              ...cssLoader,
              'less-loader'
            ]
          },
          {
            test: /\.s[ac]ss$/i,
            use: [
              ...cssLoader,
              {
                loader: 'sass-loader',
                options: {
                  additionalData: '@import \'./src/css/variable.scss\';'
                }
              }
            ]
          },
          {
            test: /\.(jpe?g|png|gif)$/i,
            use: {
              loader: 'url-loader',
              options: {
                name: '[name].[hash:8].[ext]',  // 重命名
                outputPath: 'images',    // 图片都放到output.path的images目录下
                limit: 8 * 1024 // 小于8k的生成base64
              }
            }
          },
          {
            test: /\.html$/i,
            use: 'html-loader'  // 配合html-webpack-plugin处理img标签的链接
          },
          {
            test: /\.(svg|ttf|eot|woff|woff2)/i,
            use: {
              loader: 'file-loader',
              options: {
                name: '[name].[hash:8].[ext]',
                outputPath: 'assets',
              }
            }
          },
          {
            test: /\.jsx?/i,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: [
                  [
                    '@babel/preset-env',
                    {
                      useBuiltIns: 'usage',
                      corejs: 3
                    }
                  ]
                ],
                // 下面的需要下载@babel/plugin-transform-runtiome -D和@babel/runtime -S，所以我选择上面的
                // plugins: [
                //     [
                //         '@babel/plugin-transform-runtime',
                //         {
                //             corejs: 3
                //         }
                //     ]
                // ],
                cacheDirectory: true
              }
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: resolve(__dirname, 'index.html'),
      filename: 'index.html'
    }),
    new CleanWebpackPlugin(),
    // new MiniCssExtractPlugin({
    //   filename: '[name].[hash:10].css',
    //   chunkFilename: '[id].[hash:10].css'
    // })
  ],
  devServer: {
    open: true
  },
  devtool: 'source-map'
}
