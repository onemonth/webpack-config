module.exports = {
  'env': {
    browser: true,
    node: true,
    es2021: true
  },
  'extends': 'eslint:recommended',
  'parserOptions': {
    'ecmaVersion': 12,
    'sourceType': 'module'
  },
  rules: {
    'indent': [
      'error',
      2
    ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'never'
    ],
    'block-spacing': 2,
    'array-bracket-newline': [
      2, {
        multiline: true
      }
    ],
    'key-spacing': [
      'error', {
        'beforeColon': false,
        'afterColon': true,
        mode: 'strict' }
    ],
    'no-multiple-empty-lines': [2, { max: 2, maxEOF: 1 }],
    'object-curly-spacing': ['error', 'always'],
    'comma-dangle': ['error', 'never']
  }
}

